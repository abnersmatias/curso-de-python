nota1 = float(input('Insira a primeira nota: '))
nota2 = float(input('Insira a segunda nota: '))

media = float((nota1+nota2)/2)

if media == 10.0:
    print('Aprovado com Distinção, média: ' + str(media))
elif media >= 7:
    print('Aprovado, média: ' + str(media))
elif media < 7:
    print('Reprovado, média: ' + str(media))