def ex20(lista, operacao):
    op = {
        'lista' : lista,
        'somatorio' : sum(lista),
        'tamanho' : len(lista),
        'maior' : max(lista),
        'menor' : min(lista)
        }
    
    return op[operacao]


lista = [1,2,3,4,5,6]
ex20(lista, 'maior')