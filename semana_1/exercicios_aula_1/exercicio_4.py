num1 = int(input('Digite um número inteiro '))
num2 = int(input('Digite outro número inteiro '))
num3 = float(input('Digite um número real '))

respost1 = int((num1*2) + (num2/2))
respost2 = int((num1*3) + num3)
respost3 = float(pow(num3,2))

print('O produto do dobro do primeiro com metade do segundo: ' + str(respost1))
print('A soma do triplo do primeiro com o terceiro: ' + str(respost2))
print('O terceiro elevado ao cubo: ' + str(respost3))