def ex21(lista, operacao):
    op = {
        'lista' : lista,
        'Elevado2' : list(x*x for x in lista),
        'Elevado3' : list(x*x*x for x in lista),
        }
    
    return op[operacao]


lista = [1,2,3,4,5,6]
ex21(lista, 'Elevado3')