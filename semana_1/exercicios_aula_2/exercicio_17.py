def soma_1(lista):
    for x in lista:
        lista[x-1] = lista[x-1] + 1
    return lista

def sub_1(lista):
    for x in lista:
        lista[x-1] = lista[x-1] - 1
    return lista

def aplica(func, lista):
    return func(lista)

lista1 = [1,2,3]
print(aplica(sub_1,lista1))