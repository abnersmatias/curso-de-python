def soma(x, y):
    return x + y

assert soma(2,2) == 4, 'Erro na função soma'
assert soma(0,0) == 0, 'Erro na função soma'
assert soma(3,2) == 5, 'Erro na função soma'