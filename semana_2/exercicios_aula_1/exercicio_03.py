class Calc:
    def add(self, x, y):
        return x+y
    
    def sub(self, x, y):
        return x-y
    
    def mult(self, x, y):
        return x*y
    
    def div(self, x, y):
        return x/y
    


assert Calc().add(2,2) == 4, 'Erro na função add'
assert Calc().sub(2,2) == 0, 'Erro na função sub'
assert Calc().mult(2,2) == 4, 'Erro na função mult'
assert Calc().div(2,2) == 1, 'Erro na função div'