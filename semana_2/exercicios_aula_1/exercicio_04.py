from unittest import TestCase, main
from Calc import Calc

class TestCalc(TestCase):
    def test_soma_deve_retornar_4(self):
        c = Calc()
        self.assertEquals(c.add(2,2), 4, 'Erro na função add')
    def test_sub_deve_retornar_0(self):
        c = Calc()
        self.assertEquals(c.sub(2,2), 0, 'Erro na função add')
    def test_mult_deve_retornar_4(self):
        c = Calc()
        self.assertEquals(c.mult(2,2), 4, 'Erro na função mult')
    def test_div_deve_retornar_1(self):
        c = Calc()
        self.assertEquals(c.div(2,2), 1, 'Erro na função div')