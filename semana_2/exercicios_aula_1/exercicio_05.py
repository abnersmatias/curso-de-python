from unittest import TestCase, main
from Calc import Calc

class TestCalc_soma(TestCase):

    def test_soma_deve_retornar_4(self):
        c = Calc()
        self.assertEqual(c.add(2,2), 4, 'Erro na função add teste 1')

    def test_soma_deve_retornar_0(self):
        c = Calc()
        self.assertEqual(c.add(0,0), 0, 'Erro na função add teste 2')

    def test_soma_deve_retornar_3(self):
        c = Calc()
        self.assertEqual(c.add(1,2), 3, 'Erro na função add teste 3')

    def test_soma_deve_retornar_153(self):
        c = Calc()
        self.assertEqual(c.add(33,120), 153, 'Erro na função add teste 4')

class TestCalc_sub(TestCase):

    def test_sub_deve_retornar_6(self):
        c = Calc()
        self.assertEqual(c.sub(8,2), 6, 'Erro na função sub teste 1')

    def Test_sub_deve_retornar_0(self):
        c = Calc()
        self.assertEqual(c.sub(0,0), 0, 'Erro na função sub teste 2')

    def test_sub_deve_retornar_1(self):
        c = Calc()
        self.assertEqual(c.sub(2,1), 1, 'Erro na função sub teste 3')

    def test_sub_deve_retornar_153(self):
        c = Calc()
        self.assertEqual(c.sub(200,47), 153, 'Erro na função sub teste 4')

class TestCalc_mult(TestCase):

    def test_mult_deve_retornar_6(self):
        c = Calc()
        self.assertEqual(c.mult(3,2), 6, 'Erro na função mult teste 1')

    def Test_mult_deve_retornar_0(self):
        c = Calc()
        self.assertEqual(c.mult(5,0), 0, 'Erro na função mult teste 2')

    def test_mult_deve_retornar_1(self):
        c = Calc()
        self.assertEqual(c.mult(1,1), 1, 'Erro na função mult teste 3')

    def test_mult_deve_retornar_200(self):
        c = Calc()
        self.assertEqual(c.mult(100,2), 200, 'Erro na função mult teste 4')

class TestCalc_div(TestCase):

    def test_div_deve_retornar_6(self):
        c = Calc()
        self.assertEqual(c.div(12,2), 6, 'Erro na função div teste 1')

    def Test_div_deve_retornar_0(self):
        c = Calc()
        self.assertEqual(c.div(0,0), 0, 'Erro na função div teste 2')

    def test_div_deve_retornar_1(self):
        c = Calc()
        self.assertEqual(c.div(2,2), 1, 'Erro na função div teste 3')

    def test_div_deve_retornar_50(self):
        c = Calc()
        self.assertEqual(c.div(100,2), 50, 'Erro na função div teste 4')
